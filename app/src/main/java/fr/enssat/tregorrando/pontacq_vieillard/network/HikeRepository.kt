package fr.enssat.tregorrando.pontacq_vieillard.network

import fr.enssat.tregorrando.pontacq_vieillard.model.HikeList
import fr.enssat.tregorrando.pontacq_vieillard.model.HikeDataSource
import fr.enssat.tregorrando.pontacq_vieillard.model.LoadingStatus

class HikeRepository {
    private val dataSource = HikeDataSource()

    /* fun getHikes(): HikeList {
        return dataSource.getHikingData()
    } */

    fun getHikes(hikeCallback: (HikeList, LoadingStatus) -> Unit) {
        return dataSource.getHikingData(hikeCallback)
    }
}