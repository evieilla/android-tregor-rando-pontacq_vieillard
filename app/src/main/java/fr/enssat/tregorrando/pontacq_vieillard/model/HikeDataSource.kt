package fr.enssat.tregorrando.pontacq_vieillard.model

import android.util.Log
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fr.enssat.tregorrando.pontacq_vieillard.network.HikeService
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.X509TrustManager

// Needed to bypass SSL certificate check
class TrustAllManager : X509TrustManager {
    override fun checkClientTrusted(chain: Array<out X509Certificate>?, authType: String?) {}
    override fun checkServerTrusted(chain: Array<out X509Certificate>?, authType: String?) {}
    override fun getAcceptedIssuers(): Array<X509Certificate> = arrayOf()
}

class HikeDataSource {

    // Create the moshi and retrofit objects
    private val moshi: Moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
    val hikeAdapter: JsonAdapter<HikeList> = moshi.adapter(HikeList::class.java)

    // Create the retrofit client to make the request because the API is not secure
    val client = OkHttpClient.Builder()
        .sslSocketFactory(
            SSLContext.getInstance("SSL").apply {
                init(null, arrayOf(TrustAllManager()), SecureRandom())
            }.socketFactory,
            TrustAllManager()
        )
        .hostnameVerifier(HostnameVerifier { _, _ -> true })
        .build()


    // Create the retrofit object
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://datarmor.cotesdarmor.fr/dataserver/cg22/data/")
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .client(client)
        .build()

    // Create the service used to retrieve the data
    private val service: HikeService = retrofit.create(HikeService::class.java)

    /* fun getHikingData() : HikeList {
        val response = service.getHikingData("geojson").execute()
        return response.body() ?: HikeList("featuresCollection", emptyList())
    } */

    fun getHikingData(hikeCallback: (HikeList, LoadingStatus) -> Unit) {
        // Get the data from the API
        service.getHikingData("geojson").enqueue(object : retrofit2.Callback<HikeList> {

            // If the request is successful, we get the data and we call the callback function
            override fun onResponse(call: retrofit2.Call<HikeList>, response: retrofit2.Response<HikeList>) {
                hikeCallback(response.body() ?: HikeList("featuresCollection", emptyList()), LoadingStatus.DONE)
            }

            // If the request fails, we log the error and we call the callback function
            override fun onFailure(call: retrofit2.Call<HikeList>, t: Throwable) {
                hikeCallback(HikeList("featuresCollection", emptyList()), LoadingStatus.ERROR)
                Log.d("HikeDataSource", "getHikingData: ${t.message}")
            }
        })
    }
}