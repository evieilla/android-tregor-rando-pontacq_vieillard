package fr.enssat.tregorrando.pontacq_vieillard.network

import fr.enssat.tregorrando.pontacq_vieillard.model.Hike
import fr.enssat.tregorrando.pontacq_vieillard.model.HikeList
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface HikeService {

    @GET("itineraires_randonnees_ltc")
    fun getHikingData(@Query("\$format") format: String): Call<HikeList>
}
