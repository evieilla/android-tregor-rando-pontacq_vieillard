package fr.enssat.tregorrando.pontacq_vieillard.model

import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.enssat.tregorrando.pontacq_vieillard.network.HikeRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

enum class LoadingStatus { LOADING, ERROR, DONE }

class HikeViewModel : ViewModel() {

    private val _hikeList = MutableLiveData<HikeList>()

    val hikeList: LiveData<HikeList> = _hikeList

    var loadingState = MutableStateFlow(LoadingStatus.LOADING)

    /*fun getHikeList() {
        viewModelScope.launch {
            _hikeList.value = HikeRepository().getHikes()
        }
    }*/

    fun getHikeList() {
        viewModelScope.launch {
            HikeRepository().getHikes { hikeList, loadingStatus ->
                _hikeList.value = hikeList
                loadingState.value = loadingStatus
                Log.d("HikeViewModel", "getHikeList: ${hikeList.features.size}, LoadingStatus: ${loadingState.value}")
            }
        }

        Log.d("Launched", "getHikeList: Launched")
    }
}