package fr.enssat.tregorrando.pontacq_vieillard.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass


data class HikeList (
    @Json(name = "type") val type: String,
    @Json(name = "features") val features: List<Hike>
)


data class Hike (
    @Json(name = "type") val type: String,
    @Json(name = "properties") val properties: HikeProperties,
    @Json(name = "geometry") val geometry: HikeGeometry,
)


data class HikeProperties (
    @Json(name = "id__")        val idKey: String,
    @Json(name = "iti_id")      val id: Int?,
    @Json(name = "iti_com_li")  val lieu: String?,
    @Json(name = "iti_long")    val longueur: Float?,
    @Json(name = "iti_com_in")  val insee: String?,
    @Json(name = "iti_balisa")  val balisage: String?,
    @Json(name = "iti_vocati")  val vocation: Type?,
    @Json(name = "iti_nom")     val nom: String?,
)


@JsonClass(generateAdapter = true)
data class HikeGeometry (
    @Json(name = "type") val type: String,
    @Json(name = "coordinates") val coordinates: List<List<List<Double>>>
)

enum class Type (val value: String) {
    @Json(name = "Pédestre") PEDESTRE("Pédestre"),
    @Json(name = "VTT") VTT("VTT"),
    @Json(name = "Vélo") VELO("Vélo"),
    @Json(name = "Equestre") EQUESTRE("Equestre")
}