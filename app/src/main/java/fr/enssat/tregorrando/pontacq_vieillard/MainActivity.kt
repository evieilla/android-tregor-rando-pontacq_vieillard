package fr.enssat.tregorrando.pontacq_vieillard

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.clickable
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.material.*
import androidx.compose.material.icons.*
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.window.Popup
import androidx.compose.ui.window.PopupProperties
import fr.enssat.tregorrando.pontacq_vieillard.model.Hike
import fr.enssat.tregorrando.pontacq_vieillard.model.HikeViewModel
import fr.enssat.tregorrando.pontacq_vieillard.model.LoadingStatus
import fr.enssat.tregorrando.pontacq_vieillard.model.Type
import fr.enssat.tregorrando.pontacq_vieillard.ui.theme.TregorRandoTheme
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.filter
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.MapTileProviderBasic
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Polyline


class MainActivity : ComponentActivity() {

    // Hike View Model, contains the list of hikes
    private val hikeViewModel: HikeViewModel by viewModels()

    // Current view state, used to display the hikes or the map
    private val currentView = MutableStateFlow(1)

    // popup state, used to display the popup when a hike is clicked
    private val popupShow = MutableStateFlow(false)

    // popup data state, used to populate the popup when a hike is clicked
    private val popupHike = MutableStateFlow<Hike?>(null)

    // list of hikes to display on the map
    private val toShowPathHikes = MutableStateFlow<List<Hike>>(listOf())

    // search state, used to filter the hikes
    private val searchQuery = MutableStateFlow("")

    // filter state, used to filter the hikes
    private val typeFilter =
        MutableStateFlow(listOf(Type.PEDESTRE, Type.VTT, Type.VELO, Type.EQUESTRE))


    // Main function, called when the app is launched
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Initialize the UI
        setContent {
            TregorRandoTheme {
                // Application scaffolding with a bottom bar
                Scaffold(bottomBar = { BottomBarUI() }) { innerPadding ->
                    // Main content
                    Box(Modifier.padding(innerPadding)) {
                        // Display the hikes or the map depending on the current view state
                        when (currentView.collectAsState().value) {
                            // Display the map
                            0 -> Column { OsmMapView() }

                            // Display the hikes
                            1 -> Surface(
                                Modifier.padding(10.dp, 10.dp, 10.dp, 0.dp),
                                color = MaterialTheme.colors.background
                            ) {
                                Column(Modifier.fillMaxHeight()) {
                                    // Search bar
                                    Row { HikeSearchBar() }
                                    // Hike list
                                    Row {
                                        when (hikeViewModel.loadingState.collectAsState().value) {
                                            LoadingStatus.LOADING -> HikeListLoadingUI()
                                            LoadingStatus.ERROR -> HikeListErrorUI()
                                            LoadingStatus.DONE -> HikeListUI(hikeViewModel)
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // Popup
                HikeDetailPopup()
            }
        }

        // Retrieve the hikes from the API
        hikeViewModel.getHikeList()
    }


    // ------------------ UI COMPONENTS ------------------


    // ------------------ HIKE LIST ------------------


    // Display the hikes
    @Composable
    fun HikeListUI(viewModel: HikeViewModel) {
        // Get the hikes from the view model
        val hikeListVM = remember { viewModel.hikeList.value } ?: return

        // get the filtered types
        val filter = typeFilter.collectAsState().value

        // filter the hikes
        val hikeList = MutableStateFlow(hikeListVM.features.filter { hike ->
            hike.properties.nom?.contains(searchQuery.collectAsState().value, ignoreCase = true)
                ?: false
        }.filter { hike ->
            hike.properties.vocation in filter
        })

        // collect the hikes
        val hikes = hikeList.collectAsState().value

        // display the hikes if there are some
        if (hikes.isEmpty()) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally, modifier = Modifier
                    .fillMaxSize()
                    .padding(10.dp)
            ) {
                Text("No results")
            }
        } else {
            LazyColumn {
                items(
                    count = hikes.size,
                    key = {
                        hikes[it].properties.idKey
                    }) { index ->
                    HikeCard(hikes[index])
                }
            }
        }
    }

    @Composable
    fun HikeListLoadingUI() {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator()
        }
    }

    @Composable
    fun HikeListErrorUI() {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(text = "An error occured, please check that you are connected to the internet or try again later")
        }
    }

    @Composable
    fun HikeCard(hike: Hike) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(0.dp, 0.dp, 0.dp, 10.dp)
                .clickable {
                    popupHike.value = hike
                    popupShow.value = true
                },
        ) {
            Surface(color = MaterialTheme.colors.primary) {
                Column {
                    Text(
                        text = hike.properties.nom ?: "No Name",
                        Modifier.padding(20.dp, 10.dp),
                        color = MaterialTheme.colors.onPrimary,
                    )

                    Row {
                        IconButton(onClick = {
                            // clear list and add hike
                            toShowPathHikes.value = listOf(hike)
                            currentView.value = 0
                        }) {
                            Icon(
                                imageVector = Icons.Filled.LocationOn,
                                contentDescription = "Route",
                                tint = MaterialTheme.colors.onPrimary
                            )
                        }
                        IconButton(onClick = {
                            popupShow.value = true
                            popupHike.value = hike
                        }) {
                            Icon(
                                imageVector = Icons.Filled.Info,
                                contentDescription = "Details",
                                tint = MaterialTheme.colors.onPrimary
                            )
                        }
                    }
                }
            }
        }
    }


    // ------------------ MAP ------------------

    @Composable
    fun OsmMapView() {
        val hikesPath = toShowPathHikes.collectAsState().value

        // Create the map view
        AndroidView(
            factory = { ctx ->
                // Create the map with the context
                val mapView = MapView(ctx).apply {
                    id = R.id.map
                    clipToOutline = true
                    setMultiTouchControls(true)
                    tileProvider = MapTileProviderBasic(ctx).apply {
                        tileSource = TileSourceFactory.MAPNIK
                    }
                }

                // Set the zoom and the center of the map
                mapView.controller.setZoom(14.0)

                if (hikesPath.isEmpty()) { // If no hike is selected, center the map on the ENSSAT
                    mapView.controller.setCenter(GeoPoint(48.72978287115088, -3.4623842969943768))
                } else { // Center the map on the first point of the selected hike
                    mapView.controller.setCenter(
                        GeoPoint(
                            hikesPath[0].geometry.coordinates[0][0][1],
                            hikesPath[0].geometry.coordinates[0][0][0]
                        )
                    )
                }

                // Create the overlay for the path
                for (hike in hikesPath) {

                    // For each path in the hike create a polyline to be displayed on the map
                    for (path in hike.geometry.coordinates) {
                        val pathPoints = mutableListOf<GeoPoint>()

                        for (point in path) {
                            pathPoints.add(GeoPoint(point[1], point[0]))
                        }

                        // Add the line to the map
                        mapView.overlays.add(Polyline(mapView).apply {
                            setPoints(pathPoints)
                            setOnClickListener { _, _, _ ->
                                Toast.makeText(
                                    mapView.context,
                                    "${hike.properties.nom}",
                                    Toast.LENGTH_LONG
                                ).show()
                                true
                            }
                        })
                    }
                }

                mapView
            }
        )

        // insert the user agent to be able to use the API
        Configuration.getInstance().userAgentValue = BuildConfig.APPLICATION_ID
    }




    // ------------------ HIKE SEARCH BAR ------------------

    @Composable
    fun HikeSearchBar() {
        val filter = typeFilter.collectAsState().value

        Column {
            Row { // Search bar
                TextField(
                    value = searchQuery.collectAsState().value,
                    onValueChange = { searchQuery.value = it },
                    label = { Text(text = "Search") },
                    leadingIcon = {
                        Icon(
                            imageVector = Icons.Filled.Search,
                            contentDescription = "Search"
                        )
                    },
                    singleLine = true,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(0.dp, 0.dp, 0.dp, 10.dp)
                )
            }

            // Filters
            Row(Modifier.horizontalScroll(rememberScrollState())) {
                // Iterate over enum class Type to add filter buttons
                for (type in Type.values()) {
                    Button(
                        onClick = {
                            if (filter.contains(type)) {
                                // remove type from filter
                                typeFilter.value = filter - type
                            } else {
                                // add type to filter
                                typeFilter.value = filter + type
                            }
                        },
                        modifier = Modifier.padding(0.dp, 0.dp, 10.dp, 10.dp),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = if (filter.contains(type)) {
                                MaterialTheme.colors.primary
                            } else {
                                MaterialTheme.colors.surface
                            },
                            contentColor = if (filter.contains(type)) {
                                MaterialTheme.colors.onPrimary
                            } else {
                                MaterialTheme.colors.onSurface
                            }
                        )
                    ) {
                        Text(text = type.name)
                    }
                }
            }
        }
    }


    // ------------------ Hike Popup ------------------

    @Composable
    fun HikeDetailPopup() {
        val hike = popupHike.collectAsState().value ?: return

        if (popupShow.collectAsState().value) {
            Popup(
                alignment = Alignment.Center,
                onDismissRequest = { popupShow.value = false },
                properties = PopupProperties(
                    focusable = true,
                    dismissOnBackPress = true,
                    dismissOnClickOutside = true
                )
            ) {
                Surface(
                    modifier = Modifier
                        .fillMaxSize(),
                    color = Color.Black.copy(alpha = 0.5f)
                ) {
                    Column(
                        verticalArrangement = Arrangement.Center,
                        modifier = Modifier.fillMaxWidth()
                    ) {
                        Surface(Modifier.fillMaxWidth(), color = MaterialTheme.colors.background) {
                            Column(
                                Modifier
                                    .padding(20.dp)
                                    .fillMaxWidth()
                            ) {

                                Row {
                                    Column(
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier.fillMaxWidth()
                                    ) {
                                        Text("Hike Detail", fontWeight = FontWeight.Bold)
                                    }
                                }

                                Spacer(modifier = Modifier.height(20.dp))

                                Row {
                                    Text("Name: ", fontWeight = FontWeight.Bold)
                                    Text(hike.properties.nom ?: "No information")
                                }

                                Row {
                                    Text("Location: ", fontWeight = FontWeight.Bold)
                                    Text(hike.properties.lieu ?: "No information")
                                }

                                Row {
                                    Text("Length: ", fontWeight = FontWeight.Bold)
                                    Text("" + (hike.properties.longueur ?: "No information"))
                                }

                                Row {
                                    Text("Insee: ", fontWeight = FontWeight.Bold)
                                    Text("" + (hike.properties.insee ?: "No information"))
                                }

                                Row {
                                    Text("Signposted: ", fontWeight = FontWeight.Bold)
                                    Text(hike.properties.balisage ?: "No information")
                                }

                                Row {
                                    Text("Type: ", fontWeight = FontWeight.Bold)
                                    Text(hike.properties.vocation?.value ?: "No information")
                                }

                                Spacer(modifier = Modifier.height(20.dp))

                                Row {
                                    Column(
                                        horizontalAlignment = Alignment.CenterHorizontally,
                                        modifier = Modifier.fillMaxWidth()
                                    ) {
                                        Row {
                                            Button(onClick = { popupShow.value = false }) {
                                                Text("Close")
                                            }

                                            Button(onClick = {
                                                toShowPathHikes.value = listOf(hike)
                                                currentView.value = 0
                                                popupShow.value = false
                                            }, modifier = Modifier.padding(10.dp, 0.dp)) {
                                                Text("Show on map")
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    // ------------------ BOTTOM BAR ------------------

    @Composable
    fun BottomBarUI() {
        BottomNavigation {
            BottomNavigationItem(
                icon = {
                    Icon(
                        imageVector = Icons.Default.Map,
                        contentDescription = null
                    )
                },
                label = { Text(text = "Map") },
                selected = currentView.collectAsState().value == 0,
                onClick = { setCurrentView(0) }
            )
            BottomNavigationItem(
                icon = {
                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = null
                    )
                },
                label = { Text(text = "Search") },
                selected = currentView.collectAsState().value == 1,
                onClick = { setCurrentView(1) }
            )
        }
    }

    private fun setCurrentView(view: Int) {
        currentView.value = view
    }
}