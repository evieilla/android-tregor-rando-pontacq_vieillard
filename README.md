# Trégor Rando

## Auteurs 
- PONTACQ Hugo
- VIEILLARD Edouard

## Sommaire 
1. [Introduction](#introduction)
2. [Résumé des fonctionnalités](#résumé-des-fonctionnalités)
3. [Page d'accueil](#page-daccueil)
4. [Affichage de la carte](#affichage-de-la-carte)
5. [Liste et description des randonnées](#liste-et-description-des-randonnées)
   * [Liste des randonnées](#liste-des-randonnées)
   * [Description d'une randonnée](#description-dune-randonnée)
6. [Fonctionnement](#fonctionnement)

## Introduction <a name="introduction"></a>
Trégor rando est une application Android permettant de consulter et d'afficher un itinéraire de randonnées dans le Trégor.
L'application met à votre disposition une liste de randonnées, ainsi que d'une carte permettant de visualiser l'itinéraire de la randonnée sélectionnée.

## Résumé des fonctionnalités <a name="résumé-des-fonctionnalités"></a>
 - Page d'accueil (liste des randonnées)
 - Affichage de la carte du Trégor
 - Affichage de l'itinéraire de la randonnée sur la carte
 - Liste des randonnées (filtre par type de randonnée et barre de recherche)
 - Détail d'une randonnée

## Page d'accueil <a name="page-daccueil"></a>
La page d'accueil vous permet d'accéder à la liste des randonnées, ainsi qu'à la carte du Trégor via la barre de navigation en dessous. 

<img src="README/home.jpg" style="width: 300px;"/>

## Affichage de la carte <a name="affichage-de-la-carte"></a>
L'ouverture de la carte donne un aperçu du Trégor. Vous pouvez zoomer et dézoomer, à votre convenance. 
Lorsque vous avez sélectionné une randonnée depuis la liste des randonnées, vous pouvez afficher l'itinéraire sur la carte, comme le témoigne l'illustration ci-dessous.

<img src="README/map.jpg" style="width: 300px;"/>

## Liste et description des randonnées <a name="liste-et-description-des-randonnées"></a>

### Liste des randonnées <a name="liste-des-randonnées"></a>
Vous pouvez avoir un rapide aperçu de toutes les randonnées en cliquant sur le bouton "Liste des randonnées" dans la page d'accueil. 
Vous pouvez également filtrer les randonnées en fonction de leur type (randonnée pédestre ou randonnée VTT).

<img src="README/hike-list-filter.jpg" style="width: 300px;"/>
<img src="README/hike-list-search.jpg" style="width: 300px;"/>

### Description d'une randonnée <a name="description-dune-randonnée"></a>
La description d'une randonnée donne des informations sur la randonnée, comme la distance, la durée, le type de randonnée. 
Vous pouvez également afficher l'itinéraire de la randonnée sur la carte.

<img src="README/hike-detail.jpg" style="width: 300px;"/>
